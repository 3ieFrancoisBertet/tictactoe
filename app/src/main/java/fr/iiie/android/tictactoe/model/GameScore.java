package fr.iiie.android.tictactoe.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Scores")
public class GameScore extends Model {
    @Column(name = "Winner")
    public String winner;

    public GameScore(String winner) {
        super();
        this.winner = winner;
    }

    public GameScore() {
        super();
    }

    public String getWinner() {
        return winner;
    }
}