package fr.iiie.android.tictactoe.ui.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import fr.iiie.android.tictactoe.R;
import fr.iiie.android.tictactoe.model.GameScore;

public class GameFragment extends Fragment{

    protected Resources resources;

    @InjectView(R.id.fragment_game_textview_game_infos)
    protected TextView gameInfosTextView;

    @InjectView(R.id.fragment_game_button_restart)
    protected Button restartGameButton;

    @InjectView(R.id.fragment_game_imageview_square_0_0)
    protected ImageView squareImageView00;
    @InjectView(R.id.fragment_game_imageview_square_0_1)
    protected ImageView squareImageView01;
    @InjectView(R.id.fragment_game_imageview_square_0_2)
    protected ImageView squareImageView02;
    @InjectView(R.id.fragment_game_imageview_square_1_0)
    protected ImageView squareImageView10;
    @InjectView(R.id.fragment_game_imageview_square_1_1)
    protected ImageView squareImageView11;
    @InjectView(R.id.fragment_game_imageview_square_1_2)
    protected ImageView squareImageView12;
    @InjectView(R.id.fragment_game_imageview_square_2_0)
    protected ImageView squareImageView20;
    @InjectView(R.id.fragment_game_imageview_square_2_1)
    protected ImageView squareImageView21;
    @InjectView(R.id.fragment_game_imageview_square_2_2)
    protected ImageView squareImageView22;

    public enum Player {
        NONE,
        CIRCLE,
        CROSS
    }
    protected Player gameTable[][];
    protected Player playerTurn;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_game, container, false);

        ButterKnife.inject(this, fragmentView);
        resources = getResources();
        gameTable = new Player[3][3];

        initGame();

        setHasOptionsMenu(true);
        return fragmentView;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_open_scores).setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @OnClick({R.id.fragment_game_imageview_square_0_0,
            R.id.fragment_game_imageview_square_0_1,
            R.id.fragment_game_imageview_square_0_2,
            R.id.fragment_game_imageview_square_1_0,
            R.id.fragment_game_imageview_square_1_1,
            R.id.fragment_game_imageview_square_1_2,
            R.id.fragment_game_imageview_square_2_0,
            R.id.fragment_game_imageview_square_2_1,
            R.id.fragment_game_imageview_square_2_2})
    public void onSquareClick (ImageView imgView) {
        int i = 0;
        int j = 0;

        switch (imgView.getId())
        {
            case R.id.fragment_game_imageview_square_0_0: i = 0;
                j = 0;
                break;
            case R.id.fragment_game_imageview_square_0_1: i = 0;
                j = 1;
                break;
            case R.id.fragment_game_imageview_square_0_2: i = 0;
                j = 2;
                break;
            case R.id.fragment_game_imageview_square_1_0: i = 1;
                j = 0;
                break;
            case R.id.fragment_game_imageview_square_1_1: i = 1;
                j = 1;
                break;
            case R.id.fragment_game_imageview_square_1_2: i = 1;
                j = 2;
                break;
            case R.id.fragment_game_imageview_square_2_0: i = 2;
                j = 0;
                break;
            case R.id.fragment_game_imageview_square_2_1: i = 2;
                j = 1;
                break;
            case R.id.fragment_game_imageview_square_2_2: i = 2;
                j = 2;
                break;
        }

        placePiece(i, j, imgView);
    }

    @OnClick(R.id.fragment_game_button_restart)
    public void initGame()
    {
        // Restore the default layout
        squareImageView00.setImageResource(android.R.color.transparent);
        squareImageView01.setImageResource(android.R.color.transparent);
        squareImageView02.setImageResource(android.R.color.transparent);
        squareImageView10.setImageResource(android.R.color.transparent);
        squareImageView11.setImageResource(android.R.color.transparent);
        squareImageView12.setImageResource(android.R.color.transparent);
        squareImageView20.setImageResource(android.R.color.transparent);
        squareImageView21.setImageResource(android.R.color.transparent);
        squareImageView22.setImageResource(android.R.color.transparent);


        restartGameButton.setVisibility(View.INVISIBLE);
        gameInfosTextView.setText(resources.getText(R.string.circles_turn));
        gameInfosTextView.setTextColor(resources.getColor(R.color.grey));

        playerTurn = Player.CIRCLE;
        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 3; ++j)
                gameTable[i][j] = Player.NONE;
    }

    private void endGame()
    {
        playerTurn = Player.NONE;
        restartGameButton.setVisibility(View.VISIBLE);
    }

    private void placePiece(int i, int j, ImageView squareImgView)
    {
        // Is the current square free and the game not over ?
        if (isSquareFree(i, j) && playerTurn != Player.NONE)
        {
            // Update table & set circle/cross image
            gameTable[i][j] = playerTurn;
            squareImgView.setImageDrawable((playerTurn == Player.CIRCLE ?
                    resources.getDrawable(R.drawable.ic_circle)
                    : resources.getDrawable(R.drawable.ic_cross)));

            // Is the game over ?
            if (checkVictory()) // There is a winner
            {
                if (playerTurn == Player.CIRCLE)
                {
                    gameInfosTextView.setText(resources.getText(R.string.circle_won));
                    gameInfosTextView.setTextColor(resources.getColor(R.color.blue));
                    (new GameScore("Circle")).save();
                }
                else
                {
                    gameInfosTextView.setText(resources.getText(R.string.cross_won));
                    gameInfosTextView.setTextColor(resources.getColor(R.color.red));
                    (new GameScore("Cross")).save();
                }

                endGame();
            }
            else if (checkDraw()) // There is a draw
            {
                gameInfosTextView.setText(resources.getText(R.string.draw));
                (new GameScore("Draw")).save();
                endGame();
            }
            else // Game continues
            {
                if (playerTurn == Player.CIRCLE)
                {
                    playerTurn = Player.CROSS;
                    gameInfosTextView.setText(resources.getText(R.string.crosses_turn));
                }
                else
                {
                    playerTurn = Player.CIRCLE;
                    gameInfosTextView.setText(resources.getText(R.string.circles_turn));
                }
            }
        }
    }

    private boolean isSquareFree(int i, int j)
    {
        return gameTable[i][j] == Player.NONE;
    }

    private boolean checkDraw()
    {
        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 3; ++j)
                if (gameTable[i][j] == Player.NONE)
                    return false;
        return true;
    }

    private boolean checkVictory()
    {
        return (
            // Check lines
            (gameTable[0][0] != Player.NONE && gameTable[0][0] == gameTable[0][1] && gameTable[0][1] == gameTable[0][2])
                    ||(gameTable[1][0] != Player.NONE && gameTable[1][0] == gameTable[1][1] && gameTable[1][1] == gameTable[1][2])
                    || (gameTable[2][0] != Player.NONE && gameTable[2][0] == gameTable[2][1] && gameTable[2][1] == gameTable[2][2])

                    // Check columns
                    || (gameTable[0][0] != Player.NONE && gameTable[0][0] == gameTable[1][0] && gameTable[1][0] == gameTable[2][0])
                    || (gameTable[0][1] != Player.NONE && gameTable[0][1] == gameTable[1][1] && gameTable[1][1] == gameTable[2][1])
                    || (gameTable[0][2] != Player.NONE && gameTable[0][2] == gameTable[1][2] && gameTable[1][2] == gameTable[2][2])

                    // Check diagonals
                    || (gameTable[0][0] != Player.NONE && gameTable[0][0] == gameTable[1][1] && gameTable[1][1] == gameTable[2][2])
                    || (gameTable[0][2] != Player.NONE && gameTable[0][2] == gameTable[1][1] && gameTable[1][1] == gameTable[2][0])
        );
    }

}
