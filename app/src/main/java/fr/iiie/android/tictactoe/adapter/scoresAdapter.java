package fr.iiie.android.tictactoe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import fr.iiie.android.tictactoe.R;
import fr.iiie.android.tictactoe.model.GameScore;

/**
 * Created by francois.bertet on 2/4/2015.
 */
public class ScoresAdapter extends BaseAdapter {

    List<GameScore> dataList;
    Context context;

    public ScoresAdapter(List<GameScore> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public GameScore getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView;
        ViewHolder viewHolder;

        if (convertView == null)
        {
            rowView = LayoutInflater.from(context).inflate(R.layout.list_item_score, parent, false);
            viewHolder = new ViewHolder(rowView);
            rowView.setTag(viewHolder);
        }
        else
        {
            rowView = convertView;
            viewHolder = (ViewHolder) rowView.getTag();
        }
        viewHolder.winnerTextView.setText(getItem(position).getWinner());

        return rowView;
    }

    protected class ViewHolder
    {
        @InjectView(R.id.list_item_score_textview_winner)
        protected TextView winnerTextView;
        @InjectView(R.id.list_item_score_textview_hour)
        protected TextView hourTextView;

        private ViewHolder(View v) {
            ButterKnife.inject(this, v);
        }
    }
}
