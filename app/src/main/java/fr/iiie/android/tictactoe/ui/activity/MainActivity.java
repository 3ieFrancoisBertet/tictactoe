package fr.iiie.android.tictactoe.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.activeandroid.ActiveAndroid;

import fr.iiie.android.tictactoe.R;
import fr.iiie.android.tictactoe.ui.fragment.GameFragment;
import fr.iiie.android.tictactoe.ui.fragment.ListFragment;


public class MainActivity extends ActionBarActivity {

    Fragment gameFragment, listFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gameFragment = new GameFragment();
        listFragment = new ListFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_game_container, gameFragment)
                .commit();

        ActiveAndroid.initialize(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_open_scores:
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.activity_game_container, listFragment)
                        .commit();
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                break;
            case android.R.id.home:
                getSupportFragmentManager()
                        .beginTransaction()
                        .remove(listFragment)
                        .commit();
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
