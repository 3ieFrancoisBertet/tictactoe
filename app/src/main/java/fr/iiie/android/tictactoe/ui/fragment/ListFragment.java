package fr.iiie.android.tictactoe.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Select;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import fr.iiie.android.tictactoe.R;
import fr.iiie.android.tictactoe.adapter.ScoresAdapter;
import fr.iiie.android.tictactoe.model.GameScore;

/**
 * Created by francois.bertet on 2/4/2015.
 */
public class ListFragment extends Fragment {

    @InjectView(R.id.fragment_list_textview_emptylist)
    protected TextView emptyListView;
    @InjectView(R.id.fragment_list_listview_scores)
    protected ListView scoresListView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.inject(this, fragmentView);
        scoresListView.setEmptyView(emptyListView);

        List<GameScore> scores = new Select().from(GameScore.class).execute();
        scoresListView.setAdapter(new ScoresAdapter(scores, getActivity()));

        setHasOptionsMenu(true);
        return fragmentView;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_open_scores).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }
}
